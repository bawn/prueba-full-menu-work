const express = require('express')
const app = express()
const use = require('./use')

use(app)

module.exports = app
