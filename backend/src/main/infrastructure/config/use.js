const helmet = require('helmet');
const compression = require('compression');
const setupApp = require('./setup')
const swaggerUi = require('./swagger')
const setupRoutes = require('./routes.js')

const appUse = app => {
  app.use(helmet());
  app.use(compression());
  swaggerUi(app)
  setupApp(app)
  setupRoutes(app)
}

module.exports = appUse
