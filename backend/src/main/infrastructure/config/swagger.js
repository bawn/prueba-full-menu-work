const swaggerUi = require('swagger-ui-express');
const swaggerJSDoc = require('swagger-jsdoc');

const swaggerDefinition = {
  openapi: '3.0.0',
  info: {
    title: 'Menu Work Full Stack Test',
    version: '1.0.0',
  },
};

const options = {
  swaggerDefinition,
  // Paths to files containing OpenAPI definitions
  apis: ['../routes/*.js'],
};

const swaggerSpec = swaggerJSDoc(options);

module.exports = app => app.use('/docs', swaggerUi.serve, swaggerUi.setup(swaggerSpec));
