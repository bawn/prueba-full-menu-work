const app = require('../config/express')
const appConfig = require('../config/app')
require('dotenv').config()

app.listen(appConfig.port, () => {
  console.log(`puerto: ${appConfig.port}`)
})
