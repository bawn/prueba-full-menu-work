const request = require('supertest')
const app = require('../../../../src/main/infrastructure/config/express')

describe('JSON Parser', () => {
  test('Should body as json', async () => {
    app.post('/test_json_parser', (req, res) => {
      res.send(req.body)
    })

    await request(app).post('/test_json_parser').send({ email: 'valid@email.com' }).expect({ email: 'valid@email.com' })
  })
})
